# This is my Neovim config

It is uses all the plugins I mostly need daily for python development.
For now the plugin manager is `packer` so it can be run if needed with debian provided version of Neovim.

### The minimum requirements for this config to work are:
- neovim 0.7+ preferred is 0.8+
- python
- npm
- gcc build tools
- ripgrep
- fzy - Fuzzy search

### To install:
- Run `pipenv install` in this directory with local venv creation
- Start neovim and do a `:PackerInstall`
- After a restart do `:CocInstall` and `:TSInstall`
- Now everything should be ready...
