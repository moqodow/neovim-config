local map = require"mq.utils".keymap

-- Clear highlight search on pressing <Esc> in normal mode
map("n", "<Esc>", "<cmd>nohlsearch<CR>")


-- UI Interface
map("n", "<leader>tn", "<CMD>set number!<CR>")
map("n", "<leader>tr", "<CMD>set relativenumber!<CR>")
map("n", "<leader>tw", "<CMD>set wrap!<CR>")

-- Files
map("n", "<leader>fs", "<CMD>w<CR>")

-- Buffers
map("n", "[b", "<CMD>bprevious<CR>")
map("n", "]b", "<CMD>bnext<CR>")
map("n", "<leader>bc", "<CMD>bw<CR>")
map("n", "<leader>ba", "<CMD>%bd<CR>")
map("n", "<leader>bk", "<CMD>%bd|e#<CR>")

-- Actions
map({"x", "n"}, "<leader>fF", "<CMD>Format<CR>")

-- Combine lines
map({"n", "v"}, "J", "J_")

-- Moving lines
map("n", "<C-j>", ":m .+1<CR>==")
map("n", "<C-k>", ":m .-2<CR>==")
map("i", "<C-j>", "<Esc>:m .+1<CR>==gi")
map("i", "<C-k>", "<Esc>:m .-2<CR>==gi")
map("v", "<C-j>", ":m '>+1<CR>gv=gv")
map("v", "<C-k>", ":m '<-2<CR>gv=gv")

-- System Clip board
map("x", "<C-V>", [["_dP]])
map({"n", "v"}, "<C-C>", [["+y]])
