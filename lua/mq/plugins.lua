-- Bootstrap 'packer' plugin manager
local packer_path = vim.fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"

if vim.fn.empty(vim.fn.glob(packer_path)) > 0 then
	packer_bootstrap =
		vim.fn.system({ "git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", packer_path })
	vim.cmd([[packadd packer.nvim]])
end

local status, packer = pcall(require, "packer")
if not status then
	print("Packer is not installed")
	return
end

-- Setup plugins
packer.startup(function(use)
	-- Packer can manage itself
	use"wbthomason/packer.nvim"

  -- Base
  use"tpope/vim-sensible"

  -- Helper
  use"folke/which-key.nvim"
  use"preservim/tagbar"
  use"preservim/vim-indent-guides"
  use{
    'sudormrfbin/cheatsheet.nvim',
    requires = {
      {'nvim-telescope/telescope.nvim'},
      {'nvim-lua/popup.nvim'},
      {'nvim-lua/plenary.nvim'},
    }
  }

  -- Style
  use"themercorp/themer.lua"
  use{
    "nvim-lualine/lualine.nvim",
    requires = {
      'kyazdani42/nvim-web-devicons',
      opt = true
    }
  }

  -- Productivity
  use"tpope/vim-surround"
  use"windwp/nvim-autopairs"
  use"gpanders/editorconfig.nvim"
  use"jghauser/mkdir.nvim"
  use"sbdchd/neoformat"
  use"tpope/vim-commentary"
  use"tpope/vim-abolish"
  use"easymotion/vim-easymotion"
  use"airblade/vim-gitgutter"
  use"tpope/vim-fugitive"
  use"simnalamburt/vim-mundo"

  -- Treesitter Syntax Highlighting
  use"nvim-treesitter/nvim-treesitter"

  -- Telescope
	use{
		"nvim-telescope/telescope.nvim",
		tag = "0.1.4",
		requires = { { "nvim-lua/plenary.nvim" } },
	}
  use"nvim-telescope/telescope-fzy-native.nvim"
	use"nvim-telescope/telescope-file-browser.nvim"

  -- lsp
  use"neovim/nvim-lspconfig"

  -- Lang - Auto completion
  use {
    "neoclide/coc.nvim",
    branch = "release",
  }

  if packer_bootstrap then
    packer.sync()
	end

end)
