-- Leader key
vim.g.mapleader=" "
vim.g.maplocalleader=" "
-- Set timeout length
vim.g.timeoutlen=200

-- Standard options
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.mouse = "a"
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.hlsearch = true
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.hlsearch = true

-- Line break defaults
vim.wo.wrap = false
vim.wo.linebreak = false

-- Better ui
vim.opt.cursorline = true
vim.opt.signcolumn = "yes"
vim.opt.scrolloff = 8
vim.opt.background = "dark"
vim.cmd("highlight! link SignColumn LineNr")

-- Better buffer splitting
vim.opt.splitright = true
vim.opt.splitbelow = true

-- File types
vim.opt.encoding = "utf8"
vim.opt.fileencoding = "utf8"

-- Interface response
vim.opt.updatetime = 100

-- Python environment
local python_root
-- Project specific environment setup
local py_venv = os.getenv("VIRTUAL_ENV")
if py_venv and vim.fn.isdirectory(py_venv .. "/Lib/site-packages/pynvim") then
  python_root = py_venv
else
  -- Neovim global environment setup
  local init_vim = os.getenv("MYVIMRC")
  if init_vim then
    local dirname, __ = init_vim:match('^(.*[\\/])([^\\/]-)$')
    python_root = dirname .. ".venv"
  end
end
-- Set python host prog path
if jit.os == "Linux" then
  vim.g.python3_host_prog = python_root .. "/bin/python"
end
if jit.os == "Windows" then
  vim.g.python3_host_prog = python_root .. "\\Scripts\\python.exe"
end
