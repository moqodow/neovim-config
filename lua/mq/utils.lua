-- Make it a module
local M = {}

-- Utilties
-- Merge associative arrays
function merge_tables(t1, t2)
	for key, value in pairs(t2) do
	   t1[key] = value
	end

	return t1
end

-- Reloads Neovim after file is saved
vim.cmd([[
  augroup source_nvim_config
    autocmd!
    autocmd BufWritePost '%' source <afile>
  augroup END
]])

-- Key map with default option
function M.keymap(mode, key, cmd, opts)
  opts = opts or {}
  opts = merge_tables(opts, {silent = true})
	vim.keymap.set(mode, key, cmd, opts)
end

return M
