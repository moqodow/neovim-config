vim.cmd [[
    let g:gitgutter_override_sign_column_highlight = 1
    highlight! link SignColumn LineNr
]]

local map = require"mq.utils".keymap

map("n", "]g", "<Plug>(GitGutterNextHunk)")
map("n", "[g", "<Plug>(GitGutterPrevHunk)")
map("n", "<leader>gs", "<Plug>(GitGutterStageHunk)")
map("n", "<leader>gu", "<Plug>(GitGutterUndoHunk)")
