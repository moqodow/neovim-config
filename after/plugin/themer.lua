require"themer".setup{
  colorscheme = "kanagawa",
  styles = {
    ["function"] = { style = nil },
    functionbuiltin = { style = nil },
    variable = { style = nil },
    variableBuiltIn = { style = nil },
    parameter  = { style = nil },
  },
}
