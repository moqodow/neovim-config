local map = require"mq.utils".keymap

-- coc extension list
vim.g.coc_global_extensions = {
  "@yaegassy/coc-ruff", -- python
  "coc-clangd", -- C, C++
  "coc-cmake", -- make
  "coc-json", -- json
  "coc-lua", -- lua
  "coc-powershell", -- powershell
  "coc-pydocstring", -- python
  "coc-pyright", -- python
  "coc-rust-analyzer", -- rustlang
  "coc-sh", -- posix shell
  "coc-snippets",
  "coc-toml", -- toml
  "coc-yaml", -- yaml
}

-- Some servers have issues with backup files, see #649
vim.opt.backup = false
vim.opt.writebackup = false

-- Autocomplete
function _G.check_back_space()
    local col = vim.fn.col('.') - 1
    return col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') ~= nil
end

-- Use Tab for trigger completion with characters ahead and navigate
-- NOTE: There's always a completion item selected by default, you may want to enable
-- no select by setting `"suggest.noselect": true` in your configuration file
-- NOTE: Use command ':verbose imap <tab>' to make sure Tab is not mapped by
-- other plugins before putting this into your config
local opts = {noremap = true, expr = true, replace_keycodes = false}
map("i", "<TAB>", 'coc#pum#visible() ? coc#pum#next(1) : v:lua.check_back_space() ? "<TAB>" : coc#refresh()', opts)
map("i", "<S-TAB>", [[coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"]], opts)

-- Make <CR> to accept selected completion item or notify coc.nvim to format
-- <C-g>u breaks current undo, please make your own choice
map("i", "<cr>", [[coc#pum#visible() ? coc#pum#confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"]], opts)

-- Use <c-j> to trigger snippets
map("i", "<c-j>", "<Plug>(coc-snippets-expand-jump)")
-- Use <c-space> to trigger completion
map("i", "<c-space>", "coc#refresh()", {expr = true})

-- Use `[g` and `]g` to navigate diagnostics
-- Use `:CocDiagnostics` to get all diagnostics of current buffer in location list
map("n", "[d", "<Plug>(coc-diagnostic-prev)")
map("n", "]d", "<Plug>(coc-diagnostic-next)")

-- GoTo code navigation
map("n", "<leader>cd", "<Plug>(coc-definition)")
map("n", "<leader>ct", "<Plug>(coc-type-definition)")
map("n", "<leader>ci", "<Plug>(coc-implementation)")
map("n", "<leader>cr", "<Plug>(coc-references)")


-- Use K to show documentation in preview window
function _G.show_docs()
    local cw = vim.fn.expand('<cword>')
    if vim.fn.index({'vim', 'help'}, vim.bo.filetype) >= 0 then
        vim.api.nvim_command('h ' .. cw)
    elseif vim.api.nvim_eval('coc#rpc#ready()') then
        vim.fn.CocActionAsync('doHover')
    else
        vim.api.nvim_command('!' .. vim.o.keywordprg .. ' ' .. cw)
    end
end
map("n", "<leader>hd", '<CMD>lua _G.show_docs()<CR>')


-- Highlight the symbol and its references on a CursorHold event(cursor is idle)
vim.api.nvim_create_augroup("CocGroup", {})
vim.api.nvim_create_autocmd("CursorHold", {
    group = "CocGroup",
    command = "silent call CocActionAsync('highlight')",
    desc = "Highlight symbol under cursor on CursorHold"
})


-- Symbol renaming
map("n", "<leader>rn", "<Plug>(coc-rename)")


-- Formatting selected code
map({"x", "n"}, "<leader>fS", "<Plug>(coc-format-selected)")


-- Setup formatexpr specified filetype(s)
vim.api.nvim_create_autocmd("FileType", {
    group = "CocGroup",
    pattern = "typescript,json",
    command = "setl formatexpr=CocAction('formatSelected')",
    desc = "Setup formatexpr specified filetype(s)."
})

-- Update signature help on jump placeholder
vim.api.nvim_create_autocmd("User", {
    group = "CocGroup",
    pattern = "CocJumpPlaceholder",
    command = "call CocActionAsync('showSignatureHelp')",
    desc = "Update signature help on jump placeholder"
})

-- Apply codeAction to the selected region
-- Example: `<leader>aap` for current paragraph
local opts = {nowait = true}
map({"x", "n"}, "<leader>as", "<Plug>(coc-codeaction-selected)", opts)

-- Remap keys for apply code actions at the cursor position.
map("n", "<leader>ac", "<Plug>(coc-codeaction-cursor)", opts)
-- Remap keys for apply source code actions for current file.
map("n", "<leader>as", "<Plug>(coc-codeaction-source)", opts)
-- Apply the most preferred quickfix action on the current line.
map("n", "<leader>fc", "<Plug>(coc-fix-current)", opts)

-- Remap keys for apply refactor code actions.
map("n", "<leader>rr", "<Plug>(coc-codeaction-refactor)")
map({"x", "n"}, "<leader>rs", "<Plug>(coc-codeaction-refactor-selected)")

-- Run the Code Lens actions on the current line
map("n", "<leader>al", "<Plug>(coc-codelens-action)", opts)


-- Map function and class text objects
-- NOTE: Requires 'textDocument.documentSymbol' support from the language server
map({"n", "x"}, "<leader>cof", "<Plug>(coc-funcobj-i)", opts)
map({"n", "x"}, "<leader>cof", "<Plug>(coc-funcobj-a)", opts)
map({"n", "x"}, "<leader>coc", "<Plug>(coc-classobj-i)", opts)
map({"n", "x"}, "<leader>coc", "<Plug>(coc-classobj-a)", opts)


-- Remap <C-f> and <C-b> to scroll float windows/popups
---@diagnostic disable-next-line: redefined-local
local opts = {nowait = true, expr = true}
map({"n", "v"}, "<C-f>", 'coc#float#has_scroll() ? coc#float#scroll(1) : "<C-f>"', opts)
map({"n", "v"}, "<C-b>", 'coc#float#has_scroll() ? coc#float#scroll(0) : "<C-b>"', opts)
map("i", "<C-f>", 'coc#float#has_scroll() ? "<c-r>=coc#float#scroll(1)<cr>" : "<Right>"', opts)
map("i", "<C-b>", 'coc#float#has_scroll() ? "<c-r>=coc#float#scroll(0)<cr>" : "<Left>"', opts)


-- Use CTRL-S for selections ranges
-- Requires 'textDocument/selectionRange' support of language server
map({"n", "x"}, "<C-s>", "<Plug>(coc-range-select)")


-- Add `:Format` command to format current buffer
vim.api.nvim_create_user_command("Format", "call CocAction('format')", {})

-- " Add `:Fold` command to fold current buffer
vim.api.nvim_create_user_command("Fold", "call CocAction('fold', <f-args>)", {nargs = '?'})

-- Add `:OR` command for organize imports of the current buffer
vim.api.nvim_create_user_command("OR", "call CocActionAsync('runCommand', 'editor.action.organizeImport')", {})

-- Add (Neo)Vim's native statusline support
-- NOTE: Please see `:h coc-status` for integrations with external plugins that
-- provide custom statusline: lightline.vim, vim-airline
vim.opt.statusline:prepend("%{coc#status()}%{get(b:,'coc_current_function','')}")

-- Mappings for CoCList
-- code actions and coc stuff
---@diagnostic disable-next-line: redefined-local
local opts = {nowait = true}
-- Show diagnostic on hover
map("n", "<leader>ch", "<Plug>(coc-diagnostic-info)")
-- Show all diagnostics
map("n", "<leader>cll", ":<C-u>CocList diagnostics<cr>", opts)
-- Find symbol of current document
map("n", "<leader>coo", ":<C-u>CocList outline<cr>", opts)
-- Search workspace symbols
map("n", "<leader>cs", ":<C-u>CocList -I symbols<cr>", opts)
-- Do default action for next item
map("n", "]c", ":<C-u>CocNext<cr>", opts)
-- Do default action for previous item
map("n", "[c", ":<C-u>CocPrev<cr>", opts)
-- Resume latest coc list
map("n", "<leader>clr", ":<C-u>CocListResume<cr>", opts)

-- General coc help
-- Manage extensions
map("n", "<leader>he", ":<C-u>CocList extensions<cr>", opts)
-- Show commands
map("n", "<leader>hc", ":<C-u>CocList commands<cr>", opts)
