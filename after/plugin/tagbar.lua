local map = require"mq.utils".keymap

map("n", "<leader>tt", "<CMD>TagbarToggle<CR>")
