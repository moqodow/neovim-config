local map = require "mq.utils".keymap

map("n", "<leader>gl", "<CMD>Git log --oneline<CR>")
map("n", "<leader>gg", "<CMD>Git<CR>")
map("n", "<leader>gp", ":Git push ")
