require"telescope".load_extension"fzy_native"
require"telescope".load_extension"file_browser"

-- Keybindings for telescope
local map = require"mq.utils".keymap

map("n", "<leader>bb", "<CMD>Telescope buffers<CR>")
map("n", "<leader>fr", "<CMD>Telescope oldfiles<CR>")
map("n", "<leader>ff", "<CMD>Telescope find_files hidden=true<CR>")
map("n", "<leader>fp", "<CMD>Telescope git_files<CR>")
map("n", "<leader>fb", "<CMD>Telescope file_browser hidden=true<CR>")
map("n", "<leader>fg", "<CMD>Telescope live_grep hidden=true<CR>")
map({"n", "v"}, "<leader>fs", "<CMD>Telescope grep_string hidden=true<CR>")
