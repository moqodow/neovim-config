vim.g.indent_guides_enable_on_vim_startup = true

-- Keybindings for telescope
local map = require"mq.utils".keymap

map("n", "<leader>ti", "<CMD>IndentGuidesToggle<CR>")
